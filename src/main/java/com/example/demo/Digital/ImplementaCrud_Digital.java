package com.example.demo.Digital;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Model.Digital;
import com.example.demo.Model.Analogico;

public interface ImplementaCrud_Digital extends CrudRepository<Digital,Long>  {

}
