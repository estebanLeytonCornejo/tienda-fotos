package com.example.demo.Interface;

import java.util.List;

import com.example.demo.Model.Digital;
import com.example.demo.Model.Analogico;


public interface Metodos {
	
	public List <Analogico> mostrarDatosAnalogico();
	
	public List <Digital> mostrarDatosDigital();
	
	public void guardaEnBD(Analogico analogico);
	

}
