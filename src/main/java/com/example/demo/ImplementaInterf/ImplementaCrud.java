package com.example.demo.ImplementaInterf;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.Model.Fotografia;

public interface ImplementaCrud extends CrudRepository<Fotografia,Long> {

}
