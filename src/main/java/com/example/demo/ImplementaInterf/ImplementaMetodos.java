package com.example.demo.ImplementaInterf;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Analogico.ImplementaCrud_Analogico;
import com.example.demo.Digital.ImplementaCrud_Digital;
import com.example.demo.Interface.Metodos;
import com.example.demo.Model.Digital;
import com.example.demo.Model.Fotografia;
import com.example.demo.Model.Analogico;

@Service
public class ImplementaMetodos implements Metodos {
	
	@Autowired
	ImplementaCrud_Analogico Implementa_Ana;
	@Autowired
	ImplementaCrud_Digital Implementa_Dig;



	@Override
	public List<Analogico> mostrarDatosAnalogico() {
		return (List<Analogico>) Implementa_Ana.findAll();

	}

	// insertar datos
	@Override
	public void guardaEnBD(Analogico analogico) {

		Implementa_Ana.save(analogico);
	}

	@Override
	public List<Digital> mostrarDatosDigital() {
		
		return (List<Digital>) Implementa_Dig.findAll();

}


}