package com.example.demo.Model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
//@PrimaryKeyJoinColumn(name="id")
@Table(name ="digital")

public class Digital extends Fotografia  {
	/*
	@Column(name ="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
*/	
	@Column(name="pixeles")
	private Long pixeles;
	
	@Column(name="resolucion")
	private Long resolucion;
/*
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
*/
	public Long getPixeles() {
		return pixeles;
	}

	public void setPixeles(Long pixeles) {
		this.pixeles = pixeles;
	}

	public Long getResolucion() {
		return resolucion;
	}

	public void setResolucion(Long resolucion) {
		this.resolucion = resolucion;
	}


	
	

	
	
	
	

}
