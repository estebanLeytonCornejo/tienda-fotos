package com.example.demo.Model;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "fotografia") //Con esta anotación puedo llamar a la clase Entity del mismo modo que la BBDD
@Inheritance(strategy=InheritanceType.JOINED)
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

public class Fotografia  implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	//@GeneratedValue // Como genera la llave el motor (incremental,etc)
	//@Column(name = "id")
	@Id // Esta anotación declara cual campo será el id en la BBDD
	@Column(name = "id_foto")
	private Long id;
	
	
	
	@Column(name = "fecha_creacion") // Permite llamar al campo del mismo modo que la BBDD
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern ="yyyy-MM-dd")
	private Date fechaCreacion;
	
	@Column(name = "imagen") 
	private byte[] imagen;
	

	@Column(name = "num_visitas") 
	private Long numVisitas;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Date getFechaCreacion() {
		return fechaCreacion;
	}


	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}


	public byte[] getImagen() {
		return imagen;
	}


	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}


	public Long getNumVisitas() {
		return numVisitas;
	}


	public void setNumVisitas(Long numVisitas) {
		this.numVisitas = numVisitas;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	
	
	

	

	
}
