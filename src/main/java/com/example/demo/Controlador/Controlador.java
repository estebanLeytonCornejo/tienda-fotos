package com.example.demo.Controlador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.demo.Interface.Metodos;
import com.example.demo.Model.Digital;
import com.example.demo.Model.Analogico;


@Controller
public class Controlador {
	@Autowired
	Metodos metodo;
	
	

	@GetMapping("/index")
	public String index(Model model) {
		model.addAttribute("titulo", "Bienvenido a la tienda de fotos");
		model.addAttribute("guardar", "Guardar fotógrafo");
		return "index";
	}
	

	@GetMapping("/TablaDigital")
	public String TablaDigital(Model model) {	
		model.addAttribute("tituloTabla", "TABLA DE IMAGENES");				
		model.addAttribute("digital", metodo.mostrarDatosDigital());
		return "tablaDigital";
	}
	@GetMapping("/TablaAnalogico")
	public String TablaAnalogico(Model model) {	
		model.addAttribute("tituloTabla", "TABLA DE IMAGENES");				
		model.addAttribute("analogico", metodo.mostrarDatosAnalogico());		
		return "tablaAnalogico";
	}
	
	
	
	@GetMapping("/InsertarFotografo")
	public String creaFotografo(Model model) {
		Digital digital = new Digital();
		Analogico analogico = new Analogico();
		model.addAttribute("titulo", "Guardar foto");
		model.addAttribute("digital",digital );
		model.addAttribute("analogico",analogico );
		return "InsertarFotografo";
	}
	
	@GetMapping("/inserta")
	public String guardar(Analogico analogico) { 
		metodo.guardaEnBD(analogico);
		return "redirect:TablaFotos";

	}	

}
